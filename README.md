# RxRockPaperScissor

Android app that simulates Reactive RockPaperScissor game using RxJava2.

### Prerequisite

- Download and install Java8
- Install Android Studio 3.5
- Android SDK 28.0.3

### Installation
- Clone this repository
- Open in Android Studio

### Libraries used
1. Android Appcompat library (androidx version)
2. Android Jetpack - Architecture components (androidx version)

	- Lifecycle - Viewmodel only

	- Room persistent storage library

3. Android RecyclerView, CardView and ConstraintLayout (androidx version)
4. RxJava2 and RxAndroid (Reactive Extentions)
4. Butterknife (Field and method binding for Android views)
5. Stetho (A debug bridge for Android applications)

### How to run?

1. Choose a device to run

	- Open Developer options in your phone and enable USB Debugging

	- Emulator - Create a virtual device using AVD manager in Android Studio

2. Click the 'Run' in Android Studio and select the device.

### Sample App

Download the Sample APK in the source (rxrps-app.apk)

